
## 0.2.0 [05-28-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/persistence/adapter-azure_blobs!1

---

## 0.1.1 [01-17-2022]

- Initial Commit

See commit cb32e93

---
