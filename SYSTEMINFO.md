# Azure Blob Storage

Vendor: Microsoft
Homepage: https://www.microsoft.com/

Product: Azure Blob Storage
Product Page: https://azure.microsoft.com/en-us/products/storage/blobs

## Introduction
We classify Azure Blob Storage into the Data Storage domain as Azure Blob Storage provides solutions for managing data. 

"Blob Storage is optimized for storing massive amounts of unstructured data" 
 
## Why Integrate
The Azure Blob Storage adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Azure Blob Storage. With this adapter you have the ability to perform operations such as:

- Get, Delete, Copy, or Undelete Blob

## Additional Product Documentation
The [API documents for Azure Blob Storage](https://learn.microsoft.com/en-us/rest/api/storageservices/blob-service-rest-api)