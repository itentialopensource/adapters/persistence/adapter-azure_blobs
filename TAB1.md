# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Azure_blobs System. The API that was used to build the adapter for Azure_blobs is usually available in the report directory of this adapter. The adapter utilizes the Azure_blobs API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Azure Blob Storage adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Azure Blob Storage. With this adapter you have the ability to perform operations such as:

- Get, Delete, Copy, or Undelete Blob

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
