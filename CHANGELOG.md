
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:12PM

See merge request itentialopensource/adapters/adapter-azure_blobs!11

---

## 0.4.3 [08-23-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-azure_blobs!9

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:21PM

See merge request itentialopensource/adapters/adapter-azure_blobs!8

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:34PM

See merge request itentialopensource/adapters/adapter-azure_blobs!7

---

## 0.4.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/persistence/adapter-azure_blobs!6

---

## 0.3.3 [03-29-2024]

* Changes made at 2024.03.29_10:35AM

See merge request itentialopensource/adapters/persistence/adapter-azure_blobs!5

---

## 0.3.2 [03-13-2024]

* Changes made at 2024.03.13_15:19PM

See merge request itentialopensource/adapters/persistence/adapter-azure_blobs!4

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_12:44PM

See merge request itentialopensource/adapters/persistence/adapter-azure_blobs!3

---

## 0.3.0 [01-01-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/persistence/adapter-azure_blobs!2

---

## 0.2.0 [05-28-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/persistence/adapter-azure_blobs!1

---

## 0.1.1 [01-17-2022]

- Initial Commit

See commit cb32e93

---
